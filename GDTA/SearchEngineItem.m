//
//  SearchEngineItem.m
//  GDTA
//
//  Created by max on 02/06/16.
//  Copyright © 2016 Max. All rights reserved.
//

#import "SearchEngineItem.h"

@implementation SearchEngineItem

- (instancetype _Nonnull)initWithEngineName:(NSString * _Nonnull)engineName urlPrefix:(NSString * _Nonnull)urlPrefix {
    self = [super init];
    if (self) {
        self.engineName = engineName;
        self.urlPrefix = urlPrefix;
    }
    return self;
}

@end
