//
//  RequestString.m
//  GDTA
//
//  Created by Максим Дорошенко on 05.06.16.
//  Copyright © 2016 Max. All rights reserved.
//

#import "RequestString.h"
#import "SearchEngine.h"

@interface RequestString()

@property (nonnull, nonatomic, readonly, strong) NSString *allowedCharactersString;

@end

@implementation RequestString

- (instancetype _Nullable)initWithString:(NSString * _Nullable)string {
    if (!string || string.length < 1) {
        return nil;
    } else {
        self = [super init];
        if (self) {
            _baseString = string;
            _allowedCharactersString = [string stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]];
            if([string containsString:@"https://"]) {
                _httpsString = string;
            } else {
                if ([string containsString:@" "] || ![string containsString:@"."]) {
                    _httpsString = nil;
                } else {
                    _httpsString = [@"https://" stringByAppendingFormat:@"%@", _allowedCharactersString];
                }
            }
        }
        return self;
    }
}

- (NSString * _Nullable)defaultSearchString {
    if (_allowedCharactersString) {
        NSString *curEnginePrefix = [SearchEngine sharedInstance].engines[[SearchEngine sharedInstance].curEngineIndex].urlPrefix;
        return [curEnginePrefix stringByAppendingString:_allowedCharactersString];
    } else {
        return nil;
    }
}

@end
