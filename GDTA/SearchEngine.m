//
//  SearchEngine.m
//  GDTA
//
//  Created by max on 02/06/16.
//  Copyright © 2016 Max. All rights reserved.
//

#import "SearchEngine.h"

@implementation SearchEngine

+ (instancetype _Nonnull)sharedInstance {
    static dispatch_once_t onceToken;
    static id sharedInstance;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[SearchEngine alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self makeEngines];
    }
    return self;
}

- (void)makeEngines {
    SearchEngineItem *googleEngine = [[SearchEngineItem alloc] initWithEngineName:@"Google" urlPrefix:@"https://www.google.ru/#newwindow=1&q="];
    SearchEngineItem *yahooEngine = [[SearchEngineItem alloc] initWithEngineName:@"Yahoo" urlPrefix:@"https://search.yahoo.com/search?p="];
    SearchEngineItem *yandexEngine = [[SearchEngineItem alloc] initWithEngineName:@"Yandex" urlPrefix:@"https://yandex.ru/search/?text="];
    
    self.engines = [NSArray arrayWithObjects:googleEngine, yahooEngine, yandexEngine, nil];
    self.curEngineIndex = 0;
}

@end
