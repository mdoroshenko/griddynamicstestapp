//
//  RequestString.h
//  GDTA
//
//  Created by Максим Дорошенко on 05.06.16.
//  Copyright © 2016 Max. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RequestString : NSObject

@property (nonatomic, nullable, readonly, weak) NSString *baseString;
@property (nonatomic, nullable, readonly, weak) NSString *httpsString;

- (instancetype _Nullable)initWithString:(NSString * _Nullable)string;
- (NSString * _Nullable)defaultSearchString;

@end
