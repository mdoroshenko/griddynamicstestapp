//
//  CatsURLProtocol.h
//  GDTA
//
//  Created by max on 02/06/16.
//  Copyright © 2016 Max. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CatsURLProtocol : NSURLProtocol<NSURLSessionDataDelegate, NSURLSessionTaskDelegate>

@end
