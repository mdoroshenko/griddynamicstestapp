//
//  SearchEngine.h
//  GDTA
//
//  Created by max on 02/06/16.
//  Copyright © 2016 Max. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SearchEngineItem.h"

@interface SearchEngine : NSObject

@property (nonatomic, assign) NSInteger curEngineIndex;
@property (nonatomic, nonnull, strong) NSArray<SearchEngineItem *> *engines;

+ (instancetype _Nonnull)sharedInstance;

@end
