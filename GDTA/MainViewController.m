//
//  ViewController.m
//  GDTA
//
//  Created by max on 02/06/16.
//  Copyright © 2016 Max. All rights reserved.
//

#import "MainViewController.h"
#import "RequestString.h"
#import "SearchEngine.h"
#import "CatsURLProtocol.h"
#import "FBSDKShareKit/FBSDKShareKit.h"

@interface MainViewController ()

@property (weak, nonatomic) IBOutlet UITextField *urlTextField;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) NSMutableDictionary *urlAvailability;
@property (weak, nonatomic) IBOutlet UIView *popupView;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UIButton *closePopupButton;

@end

@implementation MainViewController

static CGFloat popupVisibleY = .0f;
static CGFloat popupInvisibleY = .0f;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.urlAvailability = [[NSMutableDictionary alloc] init];
}

- (void)viewDidLayoutSubviews {
    // Setup popupView frame, at only first didlayout
    if (popupVisibleY == .0f) {
        popupVisibleY = self.popupView.frame.origin.y;
        CGRect newFrame = self.popupView.frame;
        popupInvisibleY = newFrame.origin.y + newFrame.size.height;
        newFrame.origin.y = popupInvisibleY;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.popupView setHidden:YES];
            [self.popupView setFrame:newFrame];
            [self.closePopupButton setAlpha:0.0f];
        });
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// String to request
- (void)parseString:(RequestString *)string {
    if (string) {
        if (!string.httpsString) {
            [self performSearchRequest:[string defaultSearchString]];
        } else {
            [self requestResourceAvailability:string];
        }
    }
}

- (void)performSearchRequest:(NSString *)searchString {
    if (searchString) {
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:searchString]];
        [self.webView loadRequest:request];
    }
}

// Request resource availability
- (void)requestResourceAvailability:(RequestString *)requestString {
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:requestString.httpsString]];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if (httpResponse.statusCode == 200) {
            [self.webView loadRequest:request];
        } else {
            [self performSearchRequest:[requestString defaultSearchString]];
        }
    }];
    [task resume];
}

#pragma mark - Buttons
- (IBAction)settingsButtonClicked:(id)sender {
    if (self.popupView.isHidden) {
        [self showPopup];
    } else {
        [self hidePopup];
    }
}
- (IBAction)closePupupButtonClicked:(id)sender {
    [self hidePopup];
}

- (IBAction)screenshotButtonClicked:(id)sender {
    [self shareImage:[self screenCapture]];
}

- (void)showPopup {
    // Keyboard bug fix
    CGRect fixFrame = self.popupView.frame;
    fixFrame.origin.y = popupInvisibleY;
    [self.popupView setFrame:fixFrame];
    
    [self.popupView setHidden:NO];
    [self.closePopupButton setHidden:NO];
    [UIView animateWithDuration:0.25f animations:^{
        CGRect newFrame = fixFrame;
        newFrame.origin.y = popupVisibleY;
        [self.popupView setFrame:newFrame];
        [self.closePopupButton setAlpha:0.25f];
    }];
}

- (void)hidePopup {
    [SearchEngine sharedInstance].curEngineIndex = [self.pickerView selectedRowInComponent:0];
    [UIView animateWithDuration:0.25f animations:^{
        CGRect newFrame = self.popupView.frame;
        newFrame.origin.y = popupInvisibleY;
        [self.popupView setFrame:newFrame];
        [self.closePopupButton setAlpha:0.0f];
    } completion:^(BOOL finished){
        [self.popupView setHidden:YES];
        [self.closePopupButton setHidden:YES];
    }];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.urlTextField resignFirstResponder];
    [self parseString:[[RequestString alloc] initWithString:[self.urlTextField text]]];
    return YES;
}

#pragma mark - UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    return YES;
}

#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [SearchEngine sharedInstance].engines.count;
}

#pragma mark - UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [SearchEngine sharedInstance].engines[row].engineName;
}

#pragma mark - FacebookImageShare
- (UIImage *)screenCapture {
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, NO, [UIScreen mainScreen].scale);
    } else {
        UIGraphicsBeginImageContext(self.view.bounds.size);
    }
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSData *data = UIImagePNGRepresentation(image);
    return [UIImage imageWithData:data];
}

- (void)shareImage:(UIImage *)image {
    FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
    photo.image = image;
    photo.userGenerated = YES;
    FBSDKSharePhotoContent *content = [[FBSDKSharePhotoContent alloc] init];
    content.photos = @[photo];
    [FBSDKShareDialog showFromViewController:self withContent:content delegate:nil];
}

@end
