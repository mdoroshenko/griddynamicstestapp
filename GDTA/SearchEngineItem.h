//
//  SearchEngineItem.h
//  GDTA
//
//  Created by max on 02/06/16.
//  Copyright © 2016 Max. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchEngineItem : NSObject

@property (nonnull, nonatomic, strong) NSString *engineName;
@property (nonnull, nonatomic, strong) NSString *urlPrefix;

- (instancetype _Nonnull)initWithEngineName:(NSString * _Nonnull)engineName urlPrefix:(NSString * _Nonnull)urlPrefix;

@end
