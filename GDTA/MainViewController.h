//
//  ViewController.h
//  GDTA
//
//  Created by max on 02/06/16.
//  Copyright © 2016 Max. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController <UITextFieldDelegate, UIWebViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource>


@end

