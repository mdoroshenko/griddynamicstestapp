//
//  CatsURLProtocol.m
//  GDTA
//
//  Created by max on 02/06/16.
//  Copyright © 2016 Max. All rights reserved.
//

#import "CatsURLProtocol.h"
#import "UIKit/UIKit.h"
#import "CatsRandomizer.h"

@interface CatsURLProtocol()

@property (nonatomic, strong) NSURLSessionDataTask *task;

@end

@implementation CatsURLProtocol

+ (NSURLRequest *)canonicalRequestForRequest:(NSURLRequest *)request {
    return request;
}

+ (BOOL)requestIsCacheEquivalent:(NSURLRequest *)a toRequest:(NSURLRequest *)b {
    return [super requestIsCacheEquivalent:a toRequest:b];
}

+ (BOOL)canInitWithRequest:(NSURLRequest *)request {
    return YES;
}


- (void)startLoading {
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    self.task = [session dataTaskWithRequest:self.request];
    [self.task resume];
}

- (void)stopLoading {
    [self.task cancel];
    [self setTask:nil];
}

#pragma mark - NSURLSessionDelegate
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveResponse:(NSURLResponse *)response completionHandler:(void (^)(NSURLSessionResponseDisposition))completionHandler {
    [self.client URLProtocol:self didReceiveResponse:response cacheStoragePolicy:NSURLCacheStorageAllowedInMemoryOnly];
    completionHandler(YES);
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data {
    UIImage *testImage = [UIImage imageWithData:data];
    if (testImage) {
        UIImage *rndImage = [CatsRandomizer genarateRandomImage];
        NSData *rndData = UIImagePNGRepresentation(rndImage);
        [self.client URLProtocol:self didLoadData:rndData];
    } else {
        [self.client URLProtocol:self didLoadData:data];
    }
}

#pragma mark - NSURLSessionTaskDelegate
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error  {
    if (error && error.code != NSURLErrorCancelled) {
        [self.client URLProtocol:self didFailWithError:error];
    } else {
        [self.client URLProtocolDidFinishLoading:self];
    }
}

@end
