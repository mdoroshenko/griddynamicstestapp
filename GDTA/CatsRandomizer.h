//
//  CatsRandomizerView.h
//  GDTA
//
//  Created by Максим Дорошенко on 05.06.16.
//  Copyright © 2016 Max. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CatsRandomizer : NSObject

+ (UIImage *)genarateRandomImage;

@end
