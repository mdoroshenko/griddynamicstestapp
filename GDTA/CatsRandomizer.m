//
//  CatsRandomizerView.m
//  GDTA
//
//  Created by Максим Дорошенко on 05.06.16.
//  Copyright © 2016 Max. All rights reserved.
//

#import "CatsRandomizer.h"

@implementation CatsRandomizer

+ (UIImage *)genarateRandomImage {
    CGSize size = [UIScreen mainScreen].nativeBounds.size;
    UIGraphicsBeginImageContextWithOptions(size, NO, [UIScreen mainScreen].scale);
    int rndCats = arc4random_uniform(15);
    for (int i = 0; i < rndCats + 5; i++) {
        int rndX = arc4random_uniform(size.width / 10);
        int rndY = arc4random_uniform(size.height / 10);
        int rndW = arc4random_uniform(size.width);
        int rndH = size.height / size.width * rndW;
        CGRect imageRect = CGRectMake(rndX, rndY, rndW, rndH);
        [[CatsRandomizer generateRandomCat] drawInRect:imageRect];
    }
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

+ (UIImage *)generateRandomCat {
    int rnd = arc4random_uniform(3);
    NSString *fileName = [@"cat" stringByAppendingFormat:@"%d.png", rnd];
    return [UIImage imageNamed:fileName];
}

@end
